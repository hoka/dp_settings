from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

fp = open(path.join(here, 'README.md'), encoding='utf-8')
long_description = fp.read()
fp.close()

setup(
    name="dp_settings",
    version="0.0.5",
    author="Kai Hoppert",
    author_email="kai.hoppert@gmail.com",
    description=("A table for storing settings"),
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/hoka/dp_settings',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Framework :: Django :: 2.0',
        'Framework :: Django :: 2.1',
        'Framework :: Django :: 2.2'
    ],
    install_requires=[
        'Django',
    ],
    packages=find_packages(exclude=[
        'bin', 'include', 'lib', 'local_settings.py', '.gitlab-ci.yml', '.gitignore']),
)
